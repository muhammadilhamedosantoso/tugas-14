<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function utama()
    {
        return view('welcome');
    }

    public function reg()
    {
        return view('halaman.register');
    }
    public function kirim(Request $request)
    {
        $nama = $request['fname'];
        $nama2 = $request['lname'];

        return view('halaman.home', ['nama' => $nama, 'nama2' => $nama2]);
    }
}
