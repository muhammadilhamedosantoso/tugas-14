@extends('layout.master')

@section('judul')
<h1>Sanbercode</h1>
@endsection

@section('content')
    <h2>Social Media Developer Santai Berkualitas</h2>
    <p>Belajar dan Berbagi agar hidup semakin santai dan berkualitas</p>
    <h3>Benefit join di sanbercode</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastah Sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h3>Cara bergabung di Sanbercode</h3>
    <ol>
        <li>Mengunjungi website ini</li>
        <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
        <li>Selesai!</li>
    </ol>
@endsection